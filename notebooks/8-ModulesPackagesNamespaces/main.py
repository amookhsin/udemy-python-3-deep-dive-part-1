# main.py

from common.validators import *
import common

print('*************** SELF ***************')
for k in dict(globals()).keys():
    print(k)

print('*************** COMMON ***************')
for k in common.__dict__.keys():
    print(k)

print('*************** VALIDATORS ***************')
for k in common.validators.__dict__.keys():
    print(k)

print('*************** BOOLEAN ***************')
for k in common.validators.boolean.__dict__.keys():
    print(k)
